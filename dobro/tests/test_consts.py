from base import DobroBaseTestCase as T

class Case(T):

    def test_id_match(self):
        self.match_regex('id', {
            'good': [
                '1234567890',
                '1',
            ],
            'bad': [
                '0',
                '000123',
                '',
            ]
        })

    def test_ip_match(self):
        self.match_regex('ip', {
            'good': [
                '1.1.1.1',
                '9.9.9.9',
                '99.99.99.99',
                '123.123.123.123',
                '255.255.255.255',
                '123.0.123.0',
            ],
            'bad': [
                '1.1.1.',
                '...',
                '0',
                '256.11.11.11',
                '0.0.256.0',
                '-11.11.11.11',
                '11.11.11.11 ',
                '11.11 .11.11',
                'abcdef',
            ]
        })

    def test_tag_match(self):
        self.match_regex('tag', {
            'good': [
                'abc',
                'ABc',
                '_abc',
                '_z_',
                'Z',
                '_',
                'a-b-c',
                'a--',
                'a0',
            ],
            'bad': [
                '-',
                '-abcdef',
                '0a',
                '123',
            ]
        })


    # --- Helpers ---

    def match_regex(self, regex, cases):
        pattern = eval('self.dobro.PATTERN_%s' % regex.upper())
        for s in cases['good']:
            self.assertTrue(pattern.match(s))
        for s in cases['bad']:
            self.assertFalse(pattern.match(s))
