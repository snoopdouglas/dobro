import unittest

import test_cli
import test_cmds
import test_consts
import test_helpers

def suite():
    suite = unittest.TestSuite
    load = unittest.TestLoader().loadTestsFromTestCase

    return suite(map(lambda x: load(x.Case), [
        test_cli,
        test_cmds,
        test_consts,
        test_helpers,
    ]))
