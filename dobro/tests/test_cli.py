from base import DobroBaseTestCase as T

class Case(T):
    def setUp(self):
        super(Case, self).setUp()
        self.cli = self.dobro.cli(test=True)


    def parse(self, arg_str):
        return self.cli.parse_args(arg_str.split())
