from base import DobroBaseTestCase as T

class Case(T):
    def test_quote(self):
        s = "hey"
        self.assertEqual(self.dobro.q(s), "'%s'" % s)

    def test_colour(self):
        s = "hey"
        self.assertEqual(self.dobro.colour(s), s)
        self.assertEqual(self.dobro.colour(s, self.dobro.WARN[1]),
            "\033[%sm%s\033[39m" % (self.dobro.WARN[1], s))
