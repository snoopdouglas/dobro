# Contributing to dobro

Just go ahead and open an issue or pull request.

## Development prerequisites

`setup.py` uses [pandoc](http://pandoc.org/) to convert the readme from Markdown
to reStructuredText, as PyPI prefers it. You'll therefore need to install it if
you want to use setuptools in development with dobro.

On OS X with Homebrew, this is as simple as `brew install pandoc`.
